package kr.edcan.lumihana.smsboom;

import com.orm.SugarContext;

/**
 * Created by kimok_000 on 2016-03-15.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
