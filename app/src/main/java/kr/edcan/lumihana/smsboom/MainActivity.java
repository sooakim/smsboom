package kr.edcan.lumihana.smsboom;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.SugarContext;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText number, time, text;
    private Button send;
    private ImageView reset;
    private ListView listView;
    private SmsDataAdapter dataAdapter;
    private ArrayList<SmsData> arrayList;

    private String texts="", numbers="";
    private int times = 0;
    private String sms_state = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SugarContext.init(getApplicationContext());
        setDefault();
    }

    private void setSms_state(String state) {
        this.sms_state = state;
    }

    private String getSms_state() {
        return sms_state;
    }

    private String getTexts(){
        return texts;
    }

    private String getNumbers(){
        return numbers;
    }

    private int getTimes(){
        return times;
    }

    private ArrayList<SmsData> getDefaultData(){
        ArrayList<SmsData> result = new ArrayList<>();
        List<SmsData> datas = SmsData.listAll(SmsData.class);

        if(datas.size()!=0) {
            for (int i = 0; i < datas.size(); i++) {
                SmsData data = datas.get(i);
                result.add(new SmsData(data.getText(), data.getNumber(), data.getDate(), data.getState()));
            }
            return result;
        }else{
            return  result;
        }
    }

    private void setDefault() {
        number = (EditText) findViewById(R.id.number);
        time = (EditText) findViewById(R.id.time);
        text = (EditText) findViewById(R.id.text);
        send = (Button) findViewById(R.id.send);
        reset = (ImageView) findViewById(R.id.reset);
        listView = (ListView) findViewById(R.id.listview);

        arrayList = getDefaultData();
        setList();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(number.getText().toString().equals("")||text.getText().toString().equals("")||time.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"제대로 입력했는지 확인하세요", Toast.LENGTH_SHORT).show();

                }else{
                    numbers = number.getText().toString();
                    texts = "[kr.edcan.lumihana.smsboom] "+text.getText().toString();
                    times = Integer.parseInt(time.getText().toString());

                    for (int i = 0; i < getTimes(); i++) {
                        sendSms(getNumbers().trim(), getTexts());
                    }
//                SmsSendTask task = new SmsSendTask();
//                task.execute();
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsData.deleteAll(SmsData.class);
                arrayList = new ArrayList<>();
                setList();
                Toast.makeText(getApplicationContext(), "초기화됨", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setList() {
        dataAdapter = new SmsDataAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(dataAdapter);
    }

    private void sendSms(String number, String text) {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분 ss초");
        String time = dateFormat.format(date);

        final PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT_ACTION"), 0);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED_ACTION"), 0);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK: {
                        Log.e("SMS", "성공");
                        setSms_state("성공");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE: {
                        Log.e("SMS", "실패");
                        setSms_state("실패");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_NO_SERVICE: {
                        Log.e("SMS", "서비스지역이 아님");
                        setSms_state("서비스지역이 아님");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_RADIO_OFF: {
                        Log.e("SMS", "무선이 꺼져있음");
                        setSms_state("무선이 꺼져있음");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_NULL_PDU: {
                        Log.e("SMS", "PDU NULL");
                        setSms_state("PDU NULL");
                        break;
                    }
                }
            }
        }, new IntentFilter("SMS_SENT_ACTION"));

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK: {
                        Log.e("SMS", "도착완료");
                        break;
                    }

                    case Activity.RESULT_CANCELED: {
                        Log.e("SMS", "도착실패");
                        break;
                    }
                }
            }
        }, new IntentFilter("SMS_DELIVERED_ACTION"));

        SmsManager mSmsManager = SmsManager.getDefault();
        mSmsManager.sendTextMessage(number, null, text, sentIntent, deliveredIntent);
        arrayList.add(new SmsData(text, number, time, getSms_state()));
        SmsData data = new SmsData(text, number, time, getSms_state());
        data.save();
        setSms_state(null);
        setList();
    }

    class SmsSendTask extends AsyncTask<Void, Void, Void>{
        ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(MainActivity.this ,"전송중","반복 전송 작업중...", false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < getTimes(); i++) {
                sendSms(getNumbers().trim(), getTexts());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loading.dismiss();
        }
    }
}
