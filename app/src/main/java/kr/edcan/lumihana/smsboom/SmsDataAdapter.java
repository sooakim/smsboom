package kr.edcan.lumihana.smsboom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2016-03-15.
 */
public class SmsDataAdapter extends ArrayAdapter<SmsData> {
    private LayoutInflater mInflater;

    public SmsDataAdapter(Context context, ArrayList<SmsData> object) {
        super(context, 0, object);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        if (v == null) {
            view = mInflater.inflate(R.layout.list_sms_content , null);
        } else {
            view = v;
        }
        final SmsData data = this.getItem(position);
        if (data != null) {
            TextView text = (TextView) view.findViewById(R.id.text);
            TextView number = (TextView) view.findViewById(R.id.number);
            TextView date = (TextView) view.findViewById(R.id.date);
            TextView state = (TextView) view.findViewById(R.id.state);

            text.setText(data.getText());
            number.setText(data.getNumber());
            date.setText(data.getDate());
            state.setText(data.getState());
        }
        return view;
    }
}
