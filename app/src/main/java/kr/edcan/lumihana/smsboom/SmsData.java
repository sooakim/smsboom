package kr.edcan.lumihana.smsboom;

import com.orm.SugarRecord;

/**
 * Created by kimok_000 on 2016-03-15.
 */
public class SmsData extends SugarRecord{
    private String text;
    private String number;
    private String date;
    private String state;

    public SmsData(){}

    public SmsData(
            String text,
            String number,
            String date,
            String state
    ){
        this.text = text;
        this.number = number;
        this.date = date;
        this.state = state;
    }

    public String getText(){
        return text;
    }

    public String getNumber(){
        return number;
    }

    public String getDate(){
        return date;
    }

    public String getState(){
        return state;
    }
}
